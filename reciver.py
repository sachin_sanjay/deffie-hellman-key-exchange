#! /usr/bin/env python
import socket
import random
def main():
	sec=random.randint(0,100)
	g=7
	n=1009
	res=(g**sec)%n
#	print(res)
	s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.connect(('127.0.0.1',3333))
	print(s.recv(4096).decode())
	s.send(str(res).encode())
	msg=s.recv(1024).decode()
	print("recived key {}".format(msg))
	num=int(msg)
	#print(num)
	final=(num**sec)%n
	print("final key is {}".format(final))
	s.shutdown(socket.SHUT_RDWR)
	s.close()
if (__name__ == '__main__'):
	main()
