#! /usr/bin/env python
# diffie helman key
import socket
import random
def main():
	secret=random.randint(0,10000)
	g=7 # relatively prime
	n=1009 #prime
	res=(g**secret)%n
#	print (res)
	s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.bind(('127.0.0.1',3333))
	s.listen(1)
	c,ad=s.accept()
	a='hello addr {}'.format(ad)
	a=a.encode()
	c.send(a)
	msg=c.recv(1024).decode()
	print ("key for reciver is {}".format(msg))
	c.send(str(res).encode())
	num=int(msg)
	#print(num)
	res=(num**secret)%n
	print("final key is {}".format(res))
#	s.shutdown(socket.SHUT_RDWR)
	s.close()
if (__name__ == '__main__'):
	main()
